class Employee {
    constructor(name, age, salary) {
        this._name = name;
        this._age = age;
        this._salary = salary;
    }

    get name() {
        return this._name;
    }

    set name(value) {
        this._name = value
    }

    get age() {
        return this._age;
    }

    set age(value) {
        this._age = value;
    }

    get salary() {
        return this._salary;
    }

    set salary(value) {
        this._salary = value;
    }
}

class Programmer extends Employee {
    constructor(name, age, salary, lang) {
        super(name, age, salary);
        this.lang = lang;
    }

    get salary() {
        return this._salary * 3;
    }
}

const programmer1 = new Programmer('Max', 25, 5000, 'Ukrainian, English');
const programmer2 = new Programmer('Liuda', 28, 3500, 'Ukrainian, English, Italian');
const programmer3 = new Programmer('Olha', 27, 3000, 'Ukrainian');

console.log(programmer1);
console.log(programmer1.salary);

console.log(programmer2);
console.log(programmer2.salary);

console.log(programmer3);
console.log(programmer3.salary);

